package pl.oakfusion.sst.gradle

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option


class ChangeBranch extends DefaultTask {

	private String branchName = "develop"
	private String repoDir
	private List<Repo> inputRepos

	@Input
	String getBranchName() {
		return branchName
	}

	@Option(option = "name", description = "Name of the branch to be changed to. Default is 'develop")
	void setBranchName(String name) {
		this.branchName = name
	}

	String getRepoDir() {
		return repoDir
	}

	@Input
	void setRepoDir(String repoDir) {
		this.repoDir = repoDir
	}

	List<Repo> getInputRepos() {
		return inputRepos
	}

	@Input
	void setInputRepos(List<Repo> inputRepos) {
		this.inputRepos = inputRepos
	}

	@TaskAction
	void changeBranch() {
		inputRepos.each {
			repo -> repo.changeBranch(repoDir, branchName)
		}

	}
}
